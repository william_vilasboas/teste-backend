module.exports = (Sequelize, DataTypes) => {
  const FilmActor = Sequelize.define('FilmsActor', {
    name: {
      allowNull: false,
      type: DataTypes.STRING(120),
      validate: {
        notNull: {
          msg: 'Nome é um campo requerido',
        },
      },
    },
    userId: {
      type: DataTypes.INTEGER,
    },
    filmId: {
      type: DataTypes.INTEGER,
    },
    deletedAt: {
      type: DataTypes.DATE,
      allowNull: true,
    },
  }, {
    timestamps: false,
  });

  FilmActor.associate = async function associate(models) {
    FilmActor.belongsTo(models.UserModel, {
      foreignKey: 'userId',
      as: 'user',
    });
    FilmActor.belongsTo(models.FilmModel, {
      foreignKey: 'filmId',
      as: 'film',
    });
  };

  return FilmActor;
};
