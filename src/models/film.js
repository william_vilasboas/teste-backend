const { Op } = require('sequelize');
const AppTypeException = require('../errors/AppTypeException');
const { findOrFail } = require('../libs/sequelize-help');

module.exports = (Sequelize, DataTypes) => {
  const Film = Sequelize.define('Film', {
    name: {
      allowNull: false,
      type: DataTypes.STRING(120),
      validate: {
        notNull: {
          msg: 'Nome é um campo requerido',
        },
      },
    },
    director: {
      allowNull: false,
      type: DataTypes.STRING(120),
      validate: {
        notNull: {
          msg: 'Diretor é um campo requerido',
        },
      },
    },
    gender: {
      type: DataTypes.STRING(120),
      defaultValue: 'anime',
    },
    description: {
      allowNull: false,
      type: DataTypes.STRING,
      unique: true,
      validate: {
        notNull: {
          msg: 'Descrição é um campo requerido',
        },
      },
    },
    userId: {
      type: DataTypes.INTEGER,
    },
    deletedAt: {
      type: DataTypes.DATE,
      allowNull: null,
    },
  });

  Film.associate = async function associate(models) {
    Film.belongsTo(models.UserModel, {
      foreignKey: 'userId',
      as: 'user',
    });

    Film.hasMany(models.FilmVoteModel, {
      foreignKey: 'filmId',
      as: 'votes',
    });

    Film.hasMany(models.FilmActorModel, {
      foreignKey: 'filmId',
      as: 'actors',
    });

    Film.findWithMedia = async function findWithMedia(
      { where: whereParams, term, userId },
      single = false,
    ) {
      const where = whereParams;
      const actorsInclude = {
        model: models.FilmActorModel,
        as: 'actors',
      };

      if (term) {
        where[Op.or] = [
          {
            name: {
              [Op.substring]: term,
            },
          },
          {
            gender: {
              [Op.substring]: term,
            },
          },
          {
            director: {
              [Op.substring]: term,
            },
          },
          {
            description: {
              [Op.substring]: term,
            },
          },
          {
            '$actors.name$': {
              [Op.substring]: term,
            },
          },
        ];
      }

      const films = await Film[single ? 'findOne' : 'findAll']({
        where,
        include: actorsInclude,
      });

      if (single && !films) {
        throw new AppTypeException('DATA_NOT_FOUND');
      }

      const filmsRes = single ? films.toJSON() : films;

      if (single) {
        const vote = await models.FilmVoteModel.getMedia({
          filmId: films.id,
          userId,
        });

        filmsRes.vote = vote;
      }

      return filmsRes;
    };
  };

  Film.findOrFail = (options) => findOrFail(Film)({
    ...options,
    where: {
      ...(options.where || {}),
      deletedAt: {
        [Op.is]: null,
      },
    },
  });

  return Film;
};
