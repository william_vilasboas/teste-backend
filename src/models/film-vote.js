// const AppTypeException = require('../errors/AppTypeException');

module.exports = (Sequelize, DataTypes) => {
  const FilmVote = Sequelize.define('FilmsVote', {
    vote: {
      type: DataTypes.INTEGER,
      validate: {
        min: {
          args: [0],
          msg: 'Valor não pode ser menor que 0',
        },
        max: {
          args: [4],
          msg: 'Valor não pode ser maior que 4',
        },
      },
    },
    userId: {
      type: DataTypes.INTEGER,
    },
    filmId: {
      type: DataTypes.INTEGER,
    },
    deletedAt: {
      type: DataTypes.DATE,
      allowNull: true,
    },
  }, {
    timestamps: false,
  });

  FilmVote.associate = async function associate(models) {
    FilmVote.belongsTo(models.UserModel, {
      foreignKey: 'userId',
      as: 'user',
    });
    FilmVote.belongsTo(models.FilmModel, {
      foreignKey: 'filmId',
      as: 'film',
    });

    FilmVote.findExist = async function findVote(userId, filmId) {
      await models.FilmModel.findOrFail({
        where: { id: filmId },
      });

      return FilmVote.findOne({
        where: {
          userId,
          filmId,
        },
      });
    };
  };

  FilmVote.getMedia = async function getMedia({ filmId, userId }) {
    const vote = await FilmVote.findOne({
      where: { filmId },
      attributes: [
        [Sequelize.fn('sum', Sequelize.col('vote')), 'voteSum'],
        [Sequelize.fn('count', Sequelize.col('id')), 'votes'],
        [Sequelize.literal('(sum(vote) / count(id))'), 'media'],
      ],
      group: ['filmId'],
    });

    if (vote) {
      const your = userId ? await FilmVote.findOne({
        where: { filmId, userId },
      }) : null;

      return {
        ...vote.toJSON(),
        your,
      };
    }
    return null;
  };

  return FilmVote;
};
