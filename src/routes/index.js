const express = require('express');
const AuthMiddleware = require('../middlewares/auth');
const SecureSimpleMiddleware = require('../middlewares/secure-simple');
const AuthValidation = require('../libs/auth');

const routes = express.Router();

const { Response } = require('../libs/encapsulation');
const { RuleRoot, RuleCustomerUser } = require('../libs/rule');

const AuthController = require('../controllers/auth');
const FilmController = require('../controllers/film');
const UserController = require('../controllers/user');

routes.post('/auth/sign-in', Response(AuthController.signIn));
routes.post('/auth/sign-up', Response(AuthController.signUp));

routes.get('/v1/films/:id?', AuthValidation(false), Response(FilmController.get));

SecureSimpleMiddleware(routes);
AuthMiddleware(routes);

routes.post('/user', RuleRoot, Response(AuthController.signUp));
routes.get('/user/:id?', RuleCustomerUser, Response(UserController.get));
routes.put('/user/:id?', RuleCustomerUser, Response(UserController.update));
routes.delete('/user/:id?', RuleCustomerUser, Response(UserController.remove));

routes.post('/v1/films/:id/vote', Response(FilmController.vote));

routes.post('/v1/films', RuleRoot, Response(FilmController.create));
routes.put('/v1/films/:id', RuleRoot, Response(FilmController.update));
routes.delete('/v1/films/:id', RuleRoot, Response(FilmController.remove));

module.exports = routes;
