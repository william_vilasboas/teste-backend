const {
  FilmModel, FilmVoteModel, FilmActorModel, Sequelize, sequelize,
} = require('../models');

const EasyForm = require('../libs/easy-form');
const AppTypeException = require('../errors/AppTypeException');

const get = async (req) => {
  const where = {
    deletedAt: {
      [Sequelize.Op.is]: null,
    },
  };

  if (req.params.id) {
    where.id = req.params.id;
  }

  return FilmModel.findWithMedia({
    where,
    term: req.query.term || req.header('x-filter-term'),
    userId: req.session ? req.session.user.id : null,
  }, req.params.id);
};

const create = async (req) => EasyForm(sequelize)({
  data: { userId: req.session.user.id },
  fields: req.body,
  model: FilmModel,
  subFields: {
    actors: {
      model: FilmActorModel,
      data: { userId: req.session.user.id },
    },
  },
  options: {
    sourceKey: 'filmId',
  },
});

const update = async (req) => EasyForm(sequelize)({
  data: { userId: req.session.user.id, id: req.params.id },
  fields: req.body,
  model: FilmModel,
  subFields: {
    actors: {
      model: FilmActorModel,
      data: { userId: req.session.user.id },
    },
  },
  options: {
    sourceKey: 'filmId',
  },
});

const remove = async (req) => {
  const item = await FilmModel.findOne({
    where: { id: req.params.id },
  });

  if (!item) {
    throw new AppTypeException('DATA_NOT_FOUND');
  }

  item.deletedAt = new Date();
  return item.save();
};

const vote = async (req) => {
  const { id: filmId } = req.params;

  if (await FilmVoteModel.findExist(req.session.user.id, filmId)) {
    throw new AppTypeException('VOTE_UNIQUE');
  }

  return EasyForm(sequelize)({
    data: {
      userId: req.session.user.id,
      filmId,
    },
    fields: req.body,
    model: FilmVoteModel,
  });
};

module.exports = {
  get, create, update, remove, vote,
};
