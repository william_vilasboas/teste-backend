const { app, port, name } = require('./src/app');

app.listen(port, (err) => {
  if (!err) {
    console.log(`App ${name} running in port ${port}`);
  }
});
