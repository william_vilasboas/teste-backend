module.exports = {
  async up(queryInterface) {
    return queryInterface.bulkInsert('Films', [
      {
        name: 'Slam Dunk',
        description: 'Slam dunk otimo filme de basket',
        gender: 'anime',
        userId: 1,
        director: 'Takehiko Inoue',
      }, {
        name: 'Kuroko no Basket the last game',
        description: 'Otimo filme de basket com a geração dos milagres',
        gender: 'anime',
        userId: 1,
        director: 'Shunsuke Tada',
      },
    ]);
  },

  down: async (queryInterface) => queryInterface.bulkDelete('Films', null, {}),
};
