module.exports = {
  async up(queryInterface) {
    return queryInterface.bulkInsert('FilmsActors', [
      {
        filmId: 1,
        name: 'Kuroko',
        userId: 1,
      }, {
        filmId: 2,
        name: 'Sakuragi Hanamichi',
        userId: 1,
      },
    ]);
  },

  down: async (queryInterface) => queryInterface.bulkDelete('FilmsActors', null, {}),
};
