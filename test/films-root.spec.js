const chai = require('chai');

const http = require('chai-http');

const subSet = require('chai-subset');

const { app } = require('../src/app');

const { APP_TOKEN_ROOT } = process.env;

chai.use(http);

chai.use(subSet);

const easyChai = (uri, token, method = 'get') => chai.request(app)[method](uri)
  .auth(token, {
    type: 'bearer',
  })
  .set('content-type', 'application/json')
  .type('json');

describe('Teste de integração films DELETE ROOT', () => {
  it('/v1/films Delete', (done) => {
    easyChai('/v1/films', APP_TOKEN_ROOT)
      .end((errList, resList) => {
        if (errList) {
          return done(errList);
        }

        easyChai(`/v1/films/${resList.body.pop().id}`, APP_TOKEN_ROOT, 'delete')
          .end((err, res) => {
            if (err) {
              return done(err);
            }

            chai.expect(res).to.have.status(200);
            chai.expect(res.body).to.be.a('object');
            return done();
          });
      });
  });
});
