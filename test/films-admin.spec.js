const chai = require('chai');

const http = require('chai-http');

const subSet = require('chai-subset');

const { app } = require('../src/app');

const { APP_TOKEN_ROOT } = process.env;

chai.use(http);

chai.use(subSet);

const easyChai = (uri, token, method = 'get') => chai.request(app)[method](uri)
  .auth(token, {
    type: 'bearer',
  })
  .set('content-type', 'application/json')
  .type('json');

describe('Teste de integração films ROOT', () => {
  it('/v1/films List', (done) => {
    easyChai('/v1/films', APP_TOKEN_ROOT)
      .end((err, res) => {
        if (err) {
          return done(err);
        }
        chai.expect(res).to.have.status(200);
        chai.expect(res.body).to.be.a('array');
        return done();
      });
  });

  it('/v1/films/1 Detail', (done) => {
    easyChai('/v1/films/1', APP_TOKEN_ROOT)
      .end((err, res) => {
        if (err) {
          return done(err);
        }
        chai.expect(res).to.have.status(200);
        chai.expect(res.body).to.be.a('object');
        chai.expect(res.body).to.property('name', 'Slam Dunk');
        return done();
      });
  });

  it('/v1/films Create', (done) => {
    easyChai('/v1/films', APP_TOKEN_ROOT, 'post')
      .send({
        name: 'One piece',
        description: 'One piece',
        director: 'Vilas',
        actors: [
          {
            name: 'Luffy',
          },
          {
            name: 'Zoro',
          },
        ],
      })
      .end((err, res) => {
        if (err) {
          return done(err);
        }
        chai.expect(res).to.have.status(200);
        chai.expect(res.body).to.be.a('object');
        chai.expect(res.body).to.property('name', 'One piece');
        chai.expect(res.body.actors).to.be.a('array').have.lengthOf(2);

        return done();
      });
  });

  it('/v1/films Update', (done) => {
    easyChai('/v1/films/1', APP_TOKEN_ROOT, 'put')
      .send({
        name: 'Slam Dunk',
        description: 'Slum dunk is cool',
      })
      .end((err, res) => {
        if (err) {
          return done(err);
        }

        chai.expect(res).to.have.status(200);
        chai.expect(res.body).to.be.a('object');

        chai.expect(res.body).to.property('name', 'Slam Dunk');
        chai.expect(res.body.actors).to.be.a('array').have.lengthOf(1);

        return done();
      });
  });
});
