const chai = require('chai');

const http = require('chai-http');

const subSet = require('chai-subset');

const { app } = require('../src/app');

const { APP_TOKEN_ROOT } = process.env;

chai.use(http);

chai.use(subSet);

const chaiGet = (uri, token) => chai.request(app)
  .get(uri)
  .auth(token, {
    type: 'bearer',
  })
  .set('content-type', 'application/json')
  .type('json');

describe('Teste de integração auth/sign-in', () => {
  it('/auth/login - POST ROOT', (done) => {
    chai
      .request(app)
      .post('/auth/sign-in')
      .set('content-type', 'application/json')
      .type('json')
      .send({
        username: 'admin@admin.com.br',
        password: 'admin',
      })
      .end((errAuth, resAuth) => {
        const { error } = resAuth;
        if (error || errAuth) {
          return done(error || errAuth);
        }
        chai.expect(resAuth).to.have.status(200);
        chai.expect(resAuth.body).to.have.own.property('success', true);
        return done();
      });
  });

  it('/auth/login - POST Customer', (done) => {
    chai
      .request(app)
      .post('/auth/sign-in')
      .set('content-type', 'application/json')
      .type('json')
      .send({
        username: 'william.vboas@gmail.com',
        password: 'admin',
      })
      .end((errAuth, resAuth) => {
        const { error } = resAuth;
        if (error || errAuth) {
          return done(error || errAuth);
        }
        chai.expect(resAuth).to.have.status(200);
        chai.expect(resAuth.body).to.have.own.property('success', true);
        return done();
      });
  });

  // const { token } = resAuth.body;
  // chaiGet('/v1/films', token)
  //   .end((err, res) => {
  //     if (err) {
  //       return done(err);
  //     }
  //     chai.expect(res).to.have.status(200);
  //     chai.expect(res.body).to.be.a('array');
  //   });

  // chaiGet('/v1/films/1', token)
  //   .end((err, res) => {
  //     if (err) {
  //       return done(err);
  //     }

  //     chai.expect(res).to.have.status(200);
  //     chai.expect(res.body).to.be.a('object');
  //     return done();
  //   });

  // it('/action/contact - POST', done => {
  //   chai
  //     .request(app)
  //     .post('/action/contact')
  //     .set('content-type', 'application/x-www-form-urlencoded')
  //     .type('form')
  //     .send({
  //       url_email: '',
  //       name: 'William vilas Boas',
  //       email: 'william.vboas@gmail.com',
  //       message: 'Teste de integração',
  //       phone: '71991773639',
  //       token: '23121995'
  //     })
  //     .end((err, res) => {
  //       if (err) {
  //         return done(err);
  //       }
  //       chai.expect(res.body).to.have.own.property('success', true);
  //       chai.expect(res).to.have.status(200);
  //       return done(err);
  //     });
  // });

  // it('/action/pre-booking - POST', done => {
  //   chai
  //     .request(app)
  //     .post('/action/pre-booking')
  //     .set('content-type', 'application/x-www-form-urlencoded')
  //     .type('form')
  //     .send({

  //     })
  //     .end((err, res) => {
  //       if (err) {
  //         return done(err);
  //       }
  //     });
  // });
});
