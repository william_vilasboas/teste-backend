const chai = require('chai');

const http = require('chai-http');

const subSet = require('chai-subset');

const { app } = require('../src/app');

const { APP_TOKEN_CUSTOMER: TOKEN, APP_TERM: TERM } = process.env;

chai.use(http);

chai.use(subSet);

const easyChai = (uri, token, method = 'get') => chai.request(app)[method](uri)
  .auth(token, {
    type: 'bearer',
  })
  .set('content-type', 'application/json')
  .type('json');

describe('Teste de integração films Customer', () => {
  it('/v1/films List', (done) => {
    easyChai('/v1/films', TOKEN)
      .end((err, res) => {
        if (err) {
          return done(err);
        }
        chai.expect(res).to.have.status(200);
        chai.expect(res.body).to.be.a('array');
        return done();
      });
  });

  it(`/v1/films List Search Term ${TERM}`, (done) => {
    easyChai(`/v1/films?term=${TERM}`, TOKEN)
      .end((err, res) => {
        if (err) {
          return done(err);
        }
        chai.expect(res).to.have.status(200);
        chai.expect(res.body).to.be.a('array');

        return done();
      });
  });

  it('/v1/films List Search Term Sakuragi', (done) => {
    easyChai('/v1/films?term=Saguragi', TOKEN)
      .end((err, res) => {
        if (err) {
          return done(err);
        }

        chai.expect(res).to.have.status(200);
        chai.expect(res.body).to.be.a('array');

        return done();
      });
  });

  it('/v1/films/1 Detail', (done) => {
    easyChai('/v1/films/1', TOKEN)
      .end((err, res) => {
        if (err) {
          return done(err);
        }
        chai.expect(res).to.have.status(200);
        chai.expect(res.body).to.be.a('object');
        chai.expect(res.body).to.property('name', 'Slam Dunk');
        return done();
      });
  });

  it('/v1/films/1 Vote', (done) => {
    easyChai('/v1/films', TOKEN)
      .end((errList, resList) => {
        if (errList) {
          return done(errList);
        }
        return easyChai(`/v1/films/${resList.body.pop().id}/vote`, TOKEN, 'post')
          .send({
            vote: 2,
          })
          .end((err, res) => {
            if (err) {
              return done(err);
            }
            chai.expect(res).to.have.status(200);
            chai.expect(res.body).to.be.a('object');
            chai.expect(res.body).to.property('vote', 2);
            return done();
          });
      });
  });

  it('/v1/films Create Permission valid', (done) => {
    easyChai('/v1/films', TOKEN, 'post')
      .send({
        name: 'One piece',
        description: 'One piece',
        director: 'Vilas',
        actors: [
          {
            name: 'Luffy',
          },
          {
            name: 'Zoro',
          },
        ],
      })
      .end((err, res) => {
        if (err) {
          return done(err);
        }

        chai.expect(res).to.have.status(403);
        chai.expect(res.body).to.be.a('object');
        chai.expect(res.body).to.property('type', 'PERMISSION_DENIED');

        return done();
      });
  });

  it('/v1/films Update Permission valid', (done) => {
    easyChai('/v1/films/1', TOKEN, 'put')
      .send({
        name: 'Slam Dunk',
        description: 'Slum dunk is cool',
      })
      .end((err, res) => {
        if (err) {
          return done(err);
        }

        chai.expect(res).to.have.status(403);
        chai.expect(res.body).to.be.a('object');

        chai.expect(res.body).to.property('type', 'PERMISSION_DENIED');

        return done();
      });
  });

  it('/v1/films Delete Permission valid', (done) => {
    easyChai('/v1/films', TOKEN)
      .end((errList, resList) => {
        if (errList) {
          return done(errList);
        }

        easyChai(`/v1/films/${resList.body.pop().id}`, TOKEN, 'delete')
          .end((err, res) => {
            if (err) {
              return done(err);
            }

            chai.expect(res).to.have.status(403);
            chai.expect(res.body).to.be.a('object');
            chai.expect(res.body).to.property('type', 'PERMISSION_DENIED');

            return done();
          });
      });
  });
});
